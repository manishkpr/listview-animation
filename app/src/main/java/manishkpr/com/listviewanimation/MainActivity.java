package manishkpr.com.listviewanimation;

import android.app.Activity;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;

import java.util.ArrayList;
import java.util.List;

import manishkpr.com.listviewanimation.getter_setter.ListGetterSetter;
import manishkpr.com.listviewanimation.adapters.ListViewAdapter;
import manishkpr.com.listviewanimation.common.AnimUtils;

public class MainActivity extends Activity {

    private final int total = 20;
    private final int limit = 2;

    private View headerView;
    private ListView listView;
    private int headerHeight,footerHeight;
    private boolean isShowingBox = false;
    private ImageButton back;
    LinearLayout footer, layout;
    List<ListGetterSetter> data;
    ListViewAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setUpView();
    }
    //## Function to set up View
    void setUpView(){
        headerView = (View)findViewById(R.id.headerView);
        listView = (ListView)findViewById(R.id.listView1);
        back     = (ImageButton)findViewById(R.id.back);
        footer   = (LinearLayout)findViewById(R.id.footer);
        layout = (LinearLayout)findViewById(R.id.parentLayout);
        setUpList();
        clickAction();
        YoYo.with(Techniques.SlideOutUp).duration(0).playOn(headerView);
        YoYo.with(Techniques.SlideOutDown).duration(0).playOn(footer);
    }
    //## Function to set up list
    void setUpList(){
        getHeight();
        collapseItems();
    }
    //## Function Click Handle
    void clickAction(){
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                headerToggle();
            }
        });
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if(!isShowingBox) {
                    headerToggle();
                }
            }
        });
    }
    //## Function to get Height of Header and Footer
    void getHeight(){
        final ViewTreeObserver hea = layout.getViewTreeObserver();
        hea.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                if (Build.VERSION.SDK_INT < 16) {
                    layout.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                } else {
                    layout.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                }
                headerHeight = headerView.getHeight();
            }
        });
        final ViewTreeObserver fo = footer.getViewTreeObserver();
        fo.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
        @Override public void onGlobalLayout() {
                if (Build.VERSION.SDK_INT < 16) {
                    footer.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                } else {
                    footer.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                }
                footerHeight = footer.getHeight();
        }});

    }
    //## Function to Toggle Header and Footer Animation
    void headerToggle(){
        if (isShowingBox) {
            collapseItems();
            isShowingBox = false;
            AnimUtils.slideOut(100, 0, headerView, headerHeight, listView, true);
            AnimUtils.slideOut(100, 0, footer, footerHeight, listView, false);

        } else {
            expandItems();
            isShowingBox = true;
            AnimUtils.slideIn(100, 0, headerView, headerHeight, listView, true);
            AnimUtils.slideIn(100, 0, footer, footerHeight, listView, false);
        }
    }
    //## Function to Expand ListView
    void expandItems(){
        for(int i=0;i<data.size();i++){
            data.get(i).setAnimate(true);
        }
        adapter.itemTotal = data.size();
        adapter.notifyDataSetChanged();

    }
    //## Function to Collapse ListView
    void collapseItems(){
        if(data==null) {
            data = new ArrayList<>();
            adapter = new ListViewAdapter(this, data);
            for (int i = 1; i <= total; i++) {
                data.add(new ListGetterSetter(i + " Title", i + " Title Footer"));
            }
            adapter.itemTotal = limit;
            listView.setAdapter(adapter);
        }else{
            for(int i=0;i<data.size();i++){
                data.get(i).setAnimate(false);
            }
            adapter.itemTotal = limit;
            adapter.notifyDataSetChanged();
        }

    }
}
