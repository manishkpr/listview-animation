package manishkpr.com.listviewanimation.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nineoldandroids.animation.AnimatorSet;
import com.nineoldandroids.animation.ObjectAnimator;

import java.util.List;

import manishkpr.com.listviewanimation.getter_setter.ListGetterSetter;
import manishkpr.com.listviewanimation.R;

public class ListViewAdapter extends BaseAdapter {

    Context context;
    List<ListGetterSetter> data;
    public int itemTotal;

    public ListViewAdapter(Context context,List<ListGetterSetter> data) {
        this.context = context;
        this.data    = data;
    }

    @Override
    public int getCount() {
        return itemTotal;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater layoutInflater = LayoutInflater.from(this.context);
        ViewHolder viewHolder;

        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = layoutInflater.inflate(R.layout.list_items, null);
            //## Initial Views

            viewHolder.title        = (TextView) convertView.findViewById(R.id.title);
            viewHolder.title_footer = (TextView) convertView.findViewById(R.id.title_footer);
            viewHolder.imageView    = (ImageView)convertView.findViewById(R.id.imageView);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        //## Setup Data below
        viewHolder = (ViewHolder) convertView.getTag();

        viewHolder.title.setText(data.get(position).getTitle());
        viewHolder.title_footer.setText(data.get(position).getTitle_footer());

        if(!data.get(position).isAnimate()) {
            viewHolder.title_footer.setVisibility(View.GONE);
            viewHolder.imageView.setVisibility(View.GONE);
            startAnimationX(viewHolder.title, 100,-150, 0);
            startAnimationX(viewHolder.title_footer, 200,0, -150);
            startAnimationX(viewHolder.imageView, 300,0, -150);
        }else{
            viewHolder.title_footer.setVisibility(View.VISIBLE);
            viewHolder.imageView.setVisibility(View.VISIBLE);
            startAnimationX(viewHolder.title, 100,-150, 0);
            startAnimationX(viewHolder.title_footer, 200,-150, 0);
            startAnimationX(viewHolder.imageView, 300,-150, 0);
        }

        return convertView;
    }
    void startAnimationX(View view,int duration,int x1,int x2){
        AnimatorSet set = new AnimatorSet();
        set.playTogether(
                ObjectAnimator.ofFloat(view, "translationX", x1, x2)
        );
        //ObjectAnimator.ofFloat(view, "alpha", 0, 0.1f, 1)
        set.setDuration(duration).start();
    }
    public class ViewHolder{
        TextView title,title_footer;
        ImageView imageView;
    }

}
