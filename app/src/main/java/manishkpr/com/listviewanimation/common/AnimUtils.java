package manishkpr.com.listviewanimation.common;

import android.view.View;
import android.widget.ListView;

import com.nineoldandroids.animation.AnimatorSet;
import com.nineoldandroids.animation.ObjectAnimator;


public class AnimUtils {
    public static void slideIn(int duration, int delay,View view,int viewHeight,ListView listView,boolean isHeader) {
        int lvHeight = viewHeight;
        if(isHeader){ viewHeight = -viewHeight;}
        AnimatorSet set = new AnimatorSet();
        set.playTogether(
                ObjectAnimator.ofFloat(view, "translationY", viewHeight, 0),
                ObjectAnimator.ofFloat(listView, "translationY", 0, lvHeight),
                ObjectAnimator.ofFloat(view, "alpha", 0, 0.25f, 1)
        );
        set.setStartDelay(delay);
        set.setDuration(duration).start();
    }

    public static void slideOut(int duration, int delay,View view,int viewHeight,ListView listView,boolean isHeader) {
        int lvHeight = viewHeight;
        if(isHeader){ viewHeight = -viewHeight;}
        AnimatorSet set = new AnimatorSet();
        set.playTogether(
                ObjectAnimator.ofFloat(view, "translationY", 0, viewHeight),
                ObjectAnimator.ofFloat(listView, "translationY", lvHeight, 0),
                ObjectAnimator.ofFloat(view, "alpha", 1, 1, 1)
        );
        set.setStartDelay(delay);
        set.setDuration(duration).start();
    }
}
