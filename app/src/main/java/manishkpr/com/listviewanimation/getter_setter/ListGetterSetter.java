package manishkpr.com.listviewanimation.getter_setter;

public class ListGetterSetter {
    boolean animate;
    String title,title_footer;

    public ListGetterSetter(String title, String title_footer) {
        this.title = title;
        this.title_footer = title_footer;
        this.setAnimate(false);
    }

    public boolean isAnimate() {
        return animate;
    }

    public void setAnimate(boolean animate) {
        this.animate = animate;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle_footer() {
        return title_footer;
    }

    public void setTitle_footer(String title_footer) {
        this.title_footer = title_footer;
    }
}
